<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBitacoraconsumosTable extends Migration {

	public function up()
	{
		Schema::create('bitacoraconsumos', function(Blueprint $table) {
			$table->increments('id');
			$table->datetime('fechahora');
			$table->string('folio_credito_real');
			$table->string('placas');
			$table->bigInteger('tipoplan_id')->unsigned();
			$table->string('clave_plan');
			$table->integer('meta_diaria')->unsigned();
			$table->boolean('excedente');
			$table->bigInteger('chofer_id')->unsigned();
			$table->string('chofer_rfc', 13);
			$table->string('chofer_nombre');
			$table->bigInteger('estacionservicio_id')->unsigned();
			$table->string('estacion_clave');
			$table->string('estacion_nombre');
			$table->float('precio_gasolina');
			$table->float('precio_gas');
			$table->string('concepto');
			$table->float('cantidad');
			$table->string('unidad', 3)->default('LTS');
			$table->float('precio_unitario');
			$table->float('total');
			$table->float('pago_delta_gas');
			$table->float('pago_credito_real');
			$table->bigInteger('transaccion_id')->unsigned();
			$table->string('observaciones');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('bitacoraconsumos');
	}
}