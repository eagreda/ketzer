<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstacionesservicioTable extends Migration {

	public function up()
	{
		Schema::create('estacionesservicio', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('clave_estacion');
			$table->string('nombre');
			$table->string('direccion');
			$table->string('telefono');
		});
	}

	public function down()
	{
		Schema::drop('estacionesservicio');
	}
}