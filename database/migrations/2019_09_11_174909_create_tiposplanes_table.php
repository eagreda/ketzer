<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiposplanesTable extends Migration {

	public function up()
	{
		Schema::create('tiposplanes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('clave');
			$table->string('nombre');
			$table->integer('meta_diaria')->unsigned();
			$table->string('unidad', 3)->default('LTS');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('tiposplanes');
	}
}