<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAcreditadosTable extends Migration {

	public function up()
	{
		Schema::create('acreditados', function(Blueprint $table) {
			$table->increments('id');
			$table->string('apellido_paterno');
			$table->string('apellido_materno');
			$table->string('nombre1');
			$table->string('nombre2')->nullable();
			$table->enum('genero', array('M', 'F'));
			$table->date('fecha_nacimiento');
			$table->string('rfc', 13);
			$table->string('curp', 18);
			$table->string('ocupacion');
			$table->string('nacionalidad');
			$table->string('pais_origen');
			$table->bigInteger('nacimiento_entidad_id')->unsigned();
			$table->string('domicilio_calle');
			$table->string('domicilio_numero_exterior');
			$table->string('domicilio_numero_interior');
			$table->string('domicilio_colonia');
			$table->bigInteger('municipio_id');
			$table->bigInteger('entidad_id');
			$table->integer('codigo_postal');
			$table->bigInteger('tiposvivienda_id')->unsigned();
			$table->boolean('residencia_menos_anio');
			$table->integer('dependientes_economicos');
			$table->string('telefono');
			$table->string('telefono_celular');
			$table->enum('estado_civil', array('soltero', 'casado(sociedadconyugal)', 'casado(separacionBienes)', 'divorciado', 'unionlibre', 'viudo'));
			$table->string('correo_electronico');
			$table->float('ingreso_mensual');
			$table->integer('experiencia_anios');
			$table->integer('kilometros_diarios')->unsigned();
			$table->float('gasto_diario_combustible');
			$table->integer('viajes_diarios');
			$table->integer('horas_trabajadas');
			$table->integer('dias_trabajados_semana');
			$table->boolean('otra_actividad');
			$table->string('otra_actividad_nombre');
			$table->timestamps();
			$table->softDeletes();
			$table->string('automovil');
			$table->integer('automovil_anio');
			$table->string('automovil_modelo');
			$table->string('automovil_placas');
			$table->float('automovil_kilometraje');
		});
	}

	public function down()
	{
		Schema::drop('acreditados');
	}
}