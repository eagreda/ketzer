<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferenciasTable extends Migration {

	public function up()
	{
		Schema::create('referencias', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('acreditado_id')->unsigned();
			$table->string('nombre');
			$table->string('domicilio');
			$table->string('contacto');
			$table->string('telefono');
			$table->string('horario_contacto');
			$table->string('parentesco');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('referencias');
	}
}