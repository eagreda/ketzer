<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiposviviendasTable extends Migration {

	public function up()
	{
		Schema::create('tiposviviendas', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nombre');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('tiposviviendas');
	}
}