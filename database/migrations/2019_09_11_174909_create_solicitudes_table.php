<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSolicitudesTable extends Migration {

	public function up()
	{
		Schema::create('solicitudes', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('acreditado_id')->unsigned();
			$table->date('fecha');
			$table->string('folio_credito_real');
			$table->bigInteger('estatus_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('solicitudes');
	}
}