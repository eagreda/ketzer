<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('acreditados', function(Blueprint $table) {
			$table->foreign('tiposvivienda_id')->references('id')->on('tiposviviendas')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('solicitudes', function(Blueprint $table) {
			$table->foreign('acreditado_id')->references('id')->on('acreditados')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('referencias', function(Blueprint $table) {
			$table->foreign('acreditado_id')->references('id')->on('acreditados')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('bitacoraconsumos', function(Blueprint $table) {
			$table->foreign('tipoplan_id')->references('id')->on('tiposplanes')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('bitacoraconsumos', function(Blueprint $table) {
			$table->foreign('chofer_id')->references('id')->on('choferes')
						->onDelete('no action')
						->onUpdate('no action');
		});
		Schema::table('bitacoraconsumos', function(Blueprint $table) {
			$table->foreign('estacionservicio_id')->references('id')->on('estacionesservicio')
						->onDelete('no action')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::table('acreditados', function(Blueprint $table) {
			$table->dropForeign('acreditados_tiposvivienda_id_foreign');
		});
		Schema::table('solicitudes', function(Blueprint $table) {
			$table->dropForeign('solicitudes_acreditado_id_foreign');
		});
		Schema::table('referencias', function(Blueprint $table) {
			$table->dropForeign('referencias_acreditado_id_foreign');
		});
		Schema::table('bitacoraconsumos', function(Blueprint $table) {
			$table->dropForeign('bitacoraconsumos_tipoplan_id_foreign');
		});
		Schema::table('bitacoraconsumos', function(Blueprint $table) {
			$table->dropForeign('bitacoraconsumos_chofer_id_foreign');
		});
		Schema::table('bitacoraconsumos', function(Blueprint $table) {
			$table->dropForeign('bitacoraconsumos_estacionservicio_id_foreign');
		});
	}
}