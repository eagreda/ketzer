<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChoferesTable extends Migration {

	public function up()
	{
		Schema::create('choferes', function(Blueprint $table) {
			$table->increments('id');
			$table->string('tipolicencia');
			$table->string('turno_trabajo');
			$table->string('idiomas');
			$table->string('vehiculos_maneja');
			$table->enum('genero', array('M', 'F'));
			$table->integer('experiencia_anios');
			$table->string('rfc', 13)->index();
			$table->string('curp', 18);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('choferes');
	}
}