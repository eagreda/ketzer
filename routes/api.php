<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('acreditado', 'AcreditadoController');
Route::resource('solicitud', 'SolicitudController');
Route::resource('referencia', 'ReferenciaController');
Route::resource('tipovivienda', 'TipoViviendaController');
Route::resource('chofer', 'ChoferController');
Route::resource('tipoplan', 'TipoPlanController');
Route::resource('estacionservicio', 'EstacionServicioController');
Route::resource('bitacoraconsumo', 'BitacoraConsumoController');
