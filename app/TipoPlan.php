<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoPlan extends Model 
{

    protected $table = 'tiposplanes';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('clave', 'nombre', 'meta_diaria', 'unidad');

}