<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solicitud extends Model 
{

    protected $table = 'solicitudes';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('fecha', 'folio_credito_real');

    public function acreditado()
    {
        return $this->hasOne('App\Acreditado', 'acreditado_id');
    }

}