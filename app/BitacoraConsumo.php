<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BitacoraConsumo extends Model 
{

    protected $table = 'bitacoraconsumos';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('fechahora', 'folio_credito_real', 'placas', 'tipoplan_id', 'clave_plan', 'meta_diaria', 'excedente', 'chofer_rfc', 'chofer_nombre', 'estacion_clave', 'estacion_nombre', 'precio_gasolina', 'precio_gas', 'concepto', 'cantidad', 'unidad', 'precio_unitario', 'total', 'pago_delta_gas', 'pago_credito_real', 'transaccion_id', 'observaciones');

    public function chofer()
    {
        return $this->hasOne('App\Chofer', 'chofer_id');
    }

    public function estacion()
    {
        return $this->hasOne('App\EstacionServicio', 'estacionservicio_id');
    }

    public function tipoplan()
    {
        return $this->hasOne('App\TipoPlan', 'tipoplan_id');
    }

}