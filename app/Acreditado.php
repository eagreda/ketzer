<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Acreditado extends Model 
{

    protected $table = 'acreditados';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('apellido_paterno', 'apellido_materno', 'nombre1', 'nombre2', 'genero', 'fecha_nacimiento', 'rfc', 'curp', 'ocupacion', 'nacionalidad', 'pais_origen', 'domicilio_calle', 'domicilio_numero_exterior', 'domicilio_numero_interior', 'domicilio_colonia', 'telefono', 'telefono_celular', 'estado_civil', 'correo_electronico', 'ingreso_mensual', 'kilometros_diarios', 'gasto_diario_combustible', 'viajes_diarios', 'horas_trabajadas', 'dias_trabajados_semana', 'otra_actividad', 'otra_actividad_nombre', 'automovil', 'automovil_anio', 'automovil_modelo', 'automovil_placas', 'automovil_kilometraje');

    public function solicitudes()
    {
        return $this->hasMany('App\Solicitud', 'acreditado_id');
    }

    public function referencias()
    {
        return $this->hasMany('App\Referencia', 'acreditado_id');
    }

    public function tipovivienda()
    {
        return $this->hasOne('App\TipoVivienda', 'tiposvivienda_id');
    }

}