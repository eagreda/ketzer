<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Referencia extends Model 
{

    protected $table = 'referencias';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('nombre', 'domicilio', 'contacto', 'telefono', 'horario_contacto', 'parentesco');

}