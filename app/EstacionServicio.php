<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstacionServicio extends Model 
{

    protected $table = 'estacionesservicio';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('clave_estacion', 'nombre', 'direccion', 'telefono');

}