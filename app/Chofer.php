<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chofer extends Model 
{

    protected $table = 'choferes';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('tipolicencia', 'turno_trabajo', 'idiomas', 'vehiculos_maneja', 'genero', 'experiencia_anios', 'rfc', 'curp');

}